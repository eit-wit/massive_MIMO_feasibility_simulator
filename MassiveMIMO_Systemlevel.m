clear;
clc
close all
rng default
rng('shuffle');
format long

warning('off','all')

m = 4;                 % Modulation order QPSK
k = log2(m);            % Bits per symbol

K=10;   %number of users
Mant=[1]; %number of antennas
numofbits=100;  % number of bits in each user packet
nbrOfMonteCarloRealizations=3000;
EbNoVec = 4;       % Eb/No values (dB)

%Convolutional code rate 1/2 and length 7
trellis = poly2trellis(7,[171 133]);
tbl = 32;
rate = 1/2;

%Generate binary data and convert to symbols
dataIn = randi([0 1],numofbits,K);

% Convolutionally encode the data
for i=1:K
    dataEnc(:,i) = convenc(dataIn(:,i),trellis);
end

% QAM modulate
for i=1:K
    txSig(:,i) = qammod(dataEnc(:,i),m,'InputType','bit');
end
txSig=txSig.';

BERMR = [];
BERMMSE = [];
BERZF = [];

for l=1:length(Mant)
    
    M=Mant(l)
    
    %Generate of Rayleigh fading channel realizations
    Hall = (randn(M,K,nbrOfMonteCarloRealizations)+1i*randn(M,K,nbrOfMonteCarloRealizations)).*sqrt(1/2);
    
    numErrsSoftMR=zeros(K,1);
    numErrsSoftMMSE=zeros(K,1);
    numErrsSoftZF=zeros(K,1);
    
    %Go through all channel realizations
    for n = 1:nbrOfMonteCarloRealizations
        
        
        % Convert Eb/No to SNR
        snrdB = EbNoVec + 10*log10(k*rate);
        %Extract the current channel realization
        H = Hall(:,:,n);
        
        
        %Received signal
        recMMSE=zeros(K,length(txSig));
        recZF=zeros(K,length(txSig));
        recMR=zeros(K,length(txSig));
        
        for i=1:length(txSig)
            y=H*txSig(:,i);
%             if i==1
%                 awgny=awgn(y,snrdB,'measured');
%                 noise=awgny-y;
%             end
%             y=y+noise;
            
            y=awgn(y,snrdB,'measured');
            
            %Compute normalized MR precoding
            % wMR = H'./repmat(sqrt(sum(abs(H').^2,1)),[M 1]);
            wMR=H';
            yMR=wMR*y;
            recMR(:,i)=yMR;
            
            %Compute normalized MMSE precoding
            wMMSE=(H'*H + (1/(10.^(snrdB/10)))*eye(K))\H';
            yMMSE=wMMSE*y;
            recMMSE(:,i)=yMMSE;
            
            %Compute normalized ZF precoding
            wZF=(H'*H)\H';
            yZF=wZF*y;
            recZF(:,i)=yZF;
            
        end
        
        recMR=recMR.';
        recMMSE=recMMSE.';
        recZF=recZF.';
        
        numErrsInFrameSoftMR=0;
        numErrsInFrameSoftMMSE=0;
        numErrsInFrameSoftZF=0;
        
        for i=1:K
            
            %Demodulation soft demode
            
            rxDataSoftMR = qamdemod(recMR(:,i),m,'OutputType','approxllr', ...
                'NoiseVariance',10.^(snrdB/10));
            rxDataSoftMMSE = qamdemod(recMMSE(:,i),m,'OutputType','approxllr', ...
                'NoiseVariance',10.^(snrdB/10));
            rxDataSoftZF = qamdemod(recZF(:,i),m,'OutputType','approxllr', ...
                'NoiseVariance',10.^(snrdB/10));
            
            %Decode with Viterbi soft decoding
            dataSoftMR= vitdec(rxDataSoftMR,trellis,tbl,'cont','unquant');
            dataSoftMMSE= vitdec(rxDataSoftMMSE,trellis,tbl,'cont','unquant');
            dataSoftZF = vitdec(rxDataSoftZF,trellis,tbl,'cont','unquant');
            
            % Calculate the number of bit errors in the frame. Adjust for the
            % decoding delay, which is equal to the traceback depth.
            %numErrsInFrameHard = biterr(dataIn(1:end-tbl),dataHard(tbl+1:end));
            data=dataIn(:,i);
            numErrsInFrameSoftMR  = biterr(data(1:end-tbl),dataSoftMR(tbl+1:end));
            numErrsInFrameSoftMMSE  = biterr(data(1:end-tbl),dataSoftMMSE(tbl+1:end));
            numErrsInFrameSoftZF  = biterr(data(1:end-tbl),dataSoftZF(tbl+1:end));
            
            % Increment the error and bit counters
            %numErrsHard = numErrsHard + numErrsInFrameHard;
            numErrsSoftMR(i) = numErrsSoftMR(i) + numErrsInFrameSoftMR;
            numErrsSoftMMSE(i) = numErrsSoftMMSE(i) + numErrsInFrameSoftMMSE;
            numErrsSoftZF(i) = numErrsSoftZF(i) + numErrsInFrameSoftZF;
            
            BERMR = [BERMR numErrsSoftMR(i)];
            BERMMSE = [BERMMSE numErrsSoftMMSE(i)];
            BERZF = [BERZF numErrsSoftZF(i)];
        end
        
    end
    
    totalBERMR(l)=sum(numErrsSoftMR./nbrOfMonteCarloRealizations/numofbits)/K
    totalBERMMSE(l)=sum(numErrsSoftMMSE./nbrOfMonteCarloRealizations/numofbits)/K
    totalBERZF(l)=sum(numErrsSoftZF./nbrOfMonteCarloRealizations/numofbits)/K
    
    numErrsSoftMR=0;
    numErrsSoftMMSE=0;
    numErrsSoftZF=0;
end

%%Better picturing
% for l=1:length(Mant)
%     if totalBERMMSE(l)==0
%         totalBERMMSE(l)=10^-9;
%     end
%     if totalBERZF(l)==0
%         totalBERZF(l)=10^-9;
%     end
% end

%Plot simulation results

range=Mant;
figure(1); hold on; box on;
set(gca, 'YScale', 'log')
plot(range,totalBERMR,'k--','LineWidth',1);
plot(range,totalBERMMSE,'k-s','LineWidth',1);
plot(range,totalBERZF,'ro-','LineWidth',1);
grid on
legend('MR','MMSE','ZF','Location','NorthEast');

xlabel('Number of BS Antennas (M)');
ylabel('Bit Error Rate (BER)');
title('K=10, SNR=5 dB')
ylim([0 0.5]);

figure(2); hold on; box on; 
histogram(BERMR)
histogram(BERMMSE)
histogram(BERZF)
