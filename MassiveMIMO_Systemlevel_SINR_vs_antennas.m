clear;
clc
close all
rng default
rng('shuffle');
format long

%{
  Simulation parameters
%}
nbrOfMonteCarloRealizations=3000;
% UEs
K_arr       = [10];
% Antennas
M_max       = 200;
M_incr      = 10;
%SNR
EbNo        = 4; % Start value
EbNo_min    = -10;
EbNo_incr   = 1;

%{
  System parameters
%}
%Convolutional code rate 1/2 and length 7
trellis = poly2trellis(7,[171 133]);
tbl     = 32;
rate    = 1/2;
lim_BER = 10e-9;

% Modulation
m = 4;                 % Modulation order QPSK
k = log2(m);            % Bits per symbo

% 'Frame' size
numofbits = 100;  % number of bits in each user packet

% Results
totalBERMR    = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );
totalBERMMSE  = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );
totalBERZF    = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );
min_EbNo      = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );

%{
  Simulation loop
%}
for i_k=1:length(K_arr) %For each set of number of UEs  
    K = K_arr(i_k);
    
    disp( strcat(num2str(K), ' users') )
    
    % Generate binary data and convert to symbols
    dataIn = randi([0 1],numofbits,K);

    % Convolutionally encode the data
    for i=1:K
        dataEnc(:,i) = convenc(dataIn(:,i),trellis);
    end

    txSig = [];
    % QAM modulate
    for i=1:K
        txSig(:,i) = qammod(dataEnc(:,i),m,'InputType','bit');
    end
    txSig=txSig.';
    
    for M=K:M_incr:M_max; %For each set of number of antennas
        disp( strcat(' - ', num2str(M), ' antennas') )

        while true; % While SNR not meet
            
            %Generate of Rayleigh fading channel realizations
            Hall = (randn(M,K,nbrOfMonteCarloRealizations)+1i*randn(M,K,nbrOfMonteCarloRealizations)).*sqrt(1/2);

            numErrsSoftMR=zeros(K,1);
            numErrsSoftMMSE=zeros(K,1);
            numErrsSoftZF=zeros(K,1);

            %Go through all channel realizations
            for n = 1:nbrOfMonteCarloRealizations
                
                %Convert Eb/No to SNR
                snrdB = EbNo + 10*log10(k*rate);
                %Extract the current channel realization
                H = Hall(:,:,n);

                %Received signals
                recMMSE=zeros(K,length(txSig));
                recZF=zeros(K,length(txSig));
                recMR=zeros(K,length(txSig));

                for i=1:length(txSig) %For each symbol
                    y=H*txSig(:,i);
                    
                    if i==1;
                        awgny=awgn(y,snrdB,'measured');
                        noise=awgny-y;
                    end
                    y=y+noise;

                    %y = awgn(y,snrdB,'measured');

                    %Compute normalized MR precoding
                    % wMR = H'./repmat(sqrt(sum(abs(H').^2,1)),[M 1]);
                    wMR=H';
                    yMR=wMR*y;
                    recMR(:,i)=yMR;

                    %Compute normalized MMSE precoding
                    wMMSE=(H'*H + (1/(10.^(snrdB/10)))*eye(K))\H';
                    yMMSE=wMMSE*y;
                    recMMSE(:,i)=yMMSE;

                    %Compute normalized ZF precoding
                    wZF=(H'*H)\H';
                    yZF=wZF*y;
                    recZF(:,i)=yZF;

                end

                recMR=recMR.';
                recMMSE=recMMSE.';
                recZF=recZF.';

                numErrsInFrameSoftMR=0;
                numErrsInFrameSoftMMSE=0;
                numErrsInFrameSoftZF=0;

                for i=1:K
                    %Demodulation soft demode

                    rxDataSoftMR = qamdemod(recMR(:,i),m,'OutputType','approxllr', ...
                        'NoiseVariance',10.^(snrdB/10));
                    rxDataSoftMMSE = qamdemod(recMMSE(:,i),m,'OutputType','approxllr', ...
                        'NoiseVariance',10.^(snrdB/10));
                    rxDataSoftZF = qamdemod(recZF(:,i),m,'OutputType','approxllr', ...
                        'NoiseVariance',10.^(snrdB/10));

                    %Decode with Viterbi soft decoding
                    dataSoftMR= vitdec(rxDataSoftMR,trellis,tbl,'cont','unquant');
                    dataSoftMMSE= vitdec(rxDataSoftMMSE,trellis,tbl,'cont','unquant');
                    dataSoftZF = vitdec(rxDataSoftZF,trellis,tbl,'cont','unquant');

                    % Calculate the number of bit errors in the frame. Adjust for the
                    % decoding delay, which is equal to the traceback depth.
                    %numErrsInFrameHard = biterr(dataIn(1:end-tbl),dataHard(tbl+1:end));
                    data=dataIn(:,i);
                    numErrsInFrameSoftMR  = biterr(data(1:end-tbl),dataSoftMR(tbl+1:end));
                    numErrsInFrameSoftMMSE  = biterr(data(1:end-tbl),dataSoftMMSE(tbl+1:end));
                    numErrsInFrameSoftZF  = biterr(data(1:end-tbl),dataSoftZF(tbl+1:end));

                    % Increment the error and bit counters
                    %numErrsHard = numErrsHard + numErrsInFrameHard;
                    numErrsSoftMR(i) = numErrsSoftMR(i) + numErrsInFrameSoftMR;
                    numErrsSoftMMSE(i) = numErrsSoftMMSE(i) + numErrsInFrameSoftMMSE;
                    numErrsSoftZF(i) = numErrsSoftZF(i) + numErrsInFrameSoftZF;

                end
            end
            
            BERMR = sum(numErrsSoftMR./nbrOfMonteCarloRealizations/numofbits)/K;
            BERMMSE = sum(numErrsSoftMMSE./nbrOfMonteCarloRealizations/numofbits)/K;
            BERZF = sum(numErrsSoftZF./nbrOfMonteCarloRealizations/numofbits)/K;
            
            achived_BER = min([BERMR, BERMMSE, BERZF]); % We should pick one or evaluate each one separatly. 
            
            % Check termination conditions
            if achived_BER <= lim_BER;
                % Announc success
                disp( strcat(' - ', num2str(M),':Antennas, ', num2str(K),':Users. BER(', achived_BER , ') limit reached at: ', num2str(EbNo),'dB.') )
                
                % Save values
                totalBERMR(i_k, i_m) = BERMR;
                totalBERMMSE(i_k, i_m) = BERMMSE;
                totalBERZF(i_k, i_m) = BERZF;
                min_EbNo(i_k, i_m) = EbNo;

                % Strop itteration
                break
            end
            
            % Increment SNR
            EbNo = EbNo + sign(lim_BER-achived_BER)*EbNo_incr;
            
            if EbNo >= EbNo_min;
                break
            end

        end
        
        numErrsSoftMR=0;
        numErrsSoftMMSE=0;
        numErrsSoftZF=0;
    end
end

%%Better picturing
% for l=1:length(Mant)
%     if totalBERMMSE(l)==0
%         totalBERMMSE(l)=10^-9;
%     end
%     if totalBERZF(l)==0
%         totalBERZF(l)=10^-9;
%     end
% end

%Plot simulation results
% range=M_arr;
% figure; hold on; box on;
% set(gca, 'YScale', 'log')
% plot(range,totalBERMR,'k--','LineWidth',1);
% plot(range,totalBERMMSE,'k-s','LineWidth',1);
% plot(range,totalBERZF,'ro-','LineWidth',1);
% grid on
% legend('MR','MMSE','ZF','Location','NorthEast');
% 
% xlabel('Number of BS Antennas (M)');
% ylabel('Bit Error Rate (BER)');
% title('K=10, SNR=5 dB')
% ylim([0 0.5]);