clear;
clc
close all
rng default
rng('shuffle');
format long

%{
  Simulation parameters
%}

% UEs
K_arr       = [10];
% Antennas
M_max       = 100;
M_incr      = 10;
% SNR
EbNo_arr    = -5:1:20; % Start value

%{
  System parameters
%}
%Convolutional code rate 1/2 and length 7
trellis = poly2trellis(7,[171 133]);
tbl     = 32;
rate    = 1/2;
lim_BER = 10e-12;

% Modulation
m = 4;                 % Modulation order QPSK
k = log2(m);            % Bits per symbo

% 'Frame' size
numofbitsperpacket=100;  % number of bits in each user packet
maxnumofdatabits=1e9;
nbrOfMonteCarloRealizations=maxnumofdatabits/numofbitsperpacket;

% Results
totalBERMR    = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );
totalBERMMSE  = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );
totalBERZF    = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );
min_EbNo      = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );

% Pro-coding names

procoding_names = {'MR', 'MMSE', 'ZF'};

%{
  Simulation loop
%}
tic % Start timer

for i_k=1:length(K_arr) %For each set of number of UEs
    K = K_arr(i_k);
    
    disp( strcat(num2str(K), ' users') )
    
    %     % Generate binary data and convert to symbols
    %     dataIn = randi([0 1],numofbits,K);
    %
    %     % Convolutionally encode the data
    %     txSig = [];
    %     for i=1:K
    %         dataEnc(:,i) = convenc(dataIn(:,i),trellis);
    %
    %         % QAM modulate
    %         txSig(:,i) = qammod(dataEnc(:,i),m,'InputType','bit');
    %     end
    %     txSig=txSig.';
    
    
    M_arr = K:M_incr:M_max;
    for i_m=1:length(M_arr); %For each set of number of antennas
        M = M_arr(i_m);
        disp( strcat(' - ', num2str(M), ' antennas') )
 
        for i_EbNo=1:length(EbNo_arr); % While SNR not meet
            
            EbNo = EbNo_arr(i_EbNo);
            
            %Generate of Rayleigh fading channel realizations
            %Hall = (randn(M,K,nbrOfMonteCarloRealizations)+1i*randn(M,K,nbrOfMonteCarloRealizations)).*sqrt(1/2);
            
            numErrsSoftMR=0;
            numErrsSoftMMSE=0;
            numErrsSoftZF=0;
            
            numofdatabitssent=0;
            n=0;
            %% BER calculation starts here
            sumBERMR=0;
            sumBERMMSE=0;
            sumBERZF=0;
            
            for n=1:nbrOfMonteCarloRealizations;
               
                
                %Generate binary data and convert to symbols
                dataIn = randi([0 1],numofbitsperpacket,K);
                
                
                % Convolutionally encode the data
                for i=1:K
                    dataEnc(:,i) = convenc(dataIn(:,i),trellis);
                    
                    % QAM modulate
                    
                    txSig(:,i) = qammod(dataEnc(:,i),m,'InputType','bit');
                end
                
                %Convert Eb/No to SNR
                snrdB = EbNo + 10*log10(k*rate);
                %Extract the current channel realization
                %H = Hall(:,:,n);
                
                H=(randn(M,K,1)+1i*randn(M,K,1)).*sqrt(1/2);
                %Received signals
                recMMSE=zeros(K,length(txSig));
                recZF=zeros(K,length(txSig));
                recMR=zeros(K,length(txSig));
                
                for i=1:length(txSig) %For each symbol
                    ytx=txSig(i,:).';
                    %ytx=ytx.';
                    
                    %generate noise
                    Txpwr=mean(abs(ytx).^2);
                    snrlin=10^(snrdB/10);
                    N0=Txpwr/snrlin;
                    noiseSigma=sqrt(N0/2);  %Standard deviation for AWGN Noise when x is complex
                    noise = (noiseSigma*(randn(1,M)+1i*randn(1,M))).';%computed noise
                    %noisepwr=mean(abs(noise).^2);
                    %avgsnr=avgsnr + 10*log10(Txpwr/noisepwr);
                    y=H*ytx + noise;
                    
                    %Compute normalized MR precoding
                    % wMR = H'./repmat(sqrt(sum(abs(H').^2,1)),[M 1]);
                    wMR=H';
                    yMR=wMR*y;
                    recMR(:,i)=yMR;
                    
                    %recMR(:,i)=ytx + noise; % For verification again
                    %baseline matlab code
                    %Compute normalized MMSE precoding
                    wMMSE=(H'*H + (1/(10.^(snrdB/10)))*eye(K))\H';
                    yMMSE=wMMSE*y;
                    recMMSE(:,i)=yMMSE;
                    
                    %Compute normalized ZF precoding
                    wZF=(H'*H)\H';
                    yZF=wZF*y;
                    recZF(:,i)=yZF;
                    
                end
                
                recMR=recMR.';
                recMMSE=recMMSE.';
                recZF=recZF.';
                
                numErrsInFrameSoftMR=0;
                numErrsInFrameSoftMMSE=0;
                numErrsInFrameSoftZF=0;
                
                for i=1:K
                    %Demodulation soft demode
                    
                    rxDataSoftMR = qamdemod(recMR(:,i),m,'OutputType','approxllr', ...
                        'NoiseVariance',10.^(snrdB/10));
                    rxDataSoftMMSE = qamdemod(recMMSE(:,i),m,'OutputType','approxllr', ...
                        'NoiseVariance',10.^(snrdB/10));
                    rxDataSoftZF = qamdemod(recZF(:,i),m,'OutputType','approxllr', ...
                        'NoiseVariance',10.^(snrdB/10));
                    
                    %Decode with Viterbi soft decoding
                    dataSoftMR= vitdec(rxDataSoftMR,trellis,tbl,'cont','unquant');
                    dataSoftMMSE= vitdec(rxDataSoftMMSE,trellis,tbl,'cont','unquant');
                    dataSoftZF = vitdec(rxDataSoftZF,trellis,tbl,'cont','unquant');
                    
                    % Calculate the number of bit errors in the frame. Adjust for the
                    % decoding delay, which is equal to the traceback depth.
                    %numErrsInFrameHard = biterr(dataIn(1:end-tbl),dataHard(tbl+1:end));
                    data=dataIn(:,i);
                    numErrsInFrameSoftMR  = biterr(data(1:end-tbl),dataSoftMR(tbl+1:end));
                    numErrsInFrameSoftMMSE  = biterr(data(1:end-tbl),dataSoftMMSE(tbl+1:end));
                    numErrsInFrameSoftZF  = biterr(data(1:end-tbl),dataSoftZF(tbl+1:end));
                    
                    % Increment the error and bit counters
                    %numErrsHard = numErrsHard + numErrsInFrameHard;
                    numErrsSoftMR = numErrsSoftMR + numErrsInFrameSoftMR;
                    numErrsSoftMMSE= numErrsSoftMMSE + numErrsInFrameSoftMMSE;
                    numErrsSoftZF = numErrsSoftZF + numErrsInFrameSoftZF;
                    
                end
                
                sumBERMR=sumBERMR+ numErrsSoftMR;
                sumBERMMSE=sumBERMMSE+ numErrsSoftMMSE;
                sumBERZF=sumBERZF+ numErrsSoftZF;
                
                numErrsSoftMR=0;
                numErrsSoftMMSE=0;
                numErrsSoftZF=0;

            end
            %% BER calculation ends here
            BERMR = sumBERMR/maxnumofdatabits/K;
            BERMMSE = sumBERMMSE/maxnumofdatabits/K;
            BERZF = sumBERZF/maxnumofdatabits/K;
            
            [achived_BER, type] = min([BERMR, BERMMSE, BERZF]); % We should pick one or evaluate each one separatly.
            
            % Check termination conditions
            if achived_BER <= lim_BER;
                % Announc success
                disp( strcat(num2str(M),':Antennas, ', num2str(K),': Users at: ',num2str(EbNo),'dB.') )
                disp( strcat('- Pre-coding:', procoding_names(1), ', BER:', num2str(BERMR) ));
                disp( strcat('- Pre-coding:', procoding_names(2), ', BER:', num2str(BERMMSE) ));
                disp( strcat('- Pre-coding:', procoding_names(3), ', BER:', num2str(BERZF) ));
                
                % Save values
                totalBERMR(i_k, i_m) = BERMR;
                totalBERMMSE(i_k, i_m) = BERMMSE;
                totalBERZF(i_k, i_m) = BERZF;
                min_EbNo(i_k, i_m) = EbNo;
                
                % Strop itteration
                break
            else
                disp( strcat(' - ', num2str(M),':Antennas, ', num2str(K),':Users. BER(', num2str(achived_BER) , ') limit NOT reached') )
            end
            
        end
        
%         numErrsSoftMR=0;
%         numErrsSoftMMSE=0;
%         numErrsSoftZF=0;
        toc
    end  
    
end


%%Better picturing
% for l=1:length(Mant)
%     if totalBERMMSE(l)==0
%         totalBERMMSE(l)=10^-9;
%     end
%     if totalBERZF(l)==0
%         totalBERZF(l)=10^-9;
%     end
% end

%Plot simulation results
% range=M_arr;
% figure; hold on; box on;
% set(gca, 'YScale', 'log')
% plot(range,totalBERMR,'k--','LineWidth',1);
% plot(range,totalBERMMSE,'k-s','LineWidth',1);
% plot(range,totalBERZF,'ro-','LineWidth',1);
% grid on
% legend('MR','MMSE','ZF','Location','NorthEast');
%
% xlabel('Number of BS Antennas (M)');
% ylabel('Bit Error Rate (BER)');
% title('K=10, SNR=5 dB')
% ylim([0 0.5]);