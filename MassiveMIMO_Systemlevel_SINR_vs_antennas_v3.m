clear;
clc
close all
rng default
rng('shuffle');
format long

%{
  Simulation parameters
%}

% UEs
K_arr       = [10];
% Antennas
M_max       = 300;
M_incr      = 5;
% SNR
EbNo_arr    = -15:5:10; % Start value

%{
  System parameters
%}
%Convolutional code rate 1/2 and length 7
trellis = poly2trellis(7,[171 133]);
tbl     = 32;
rate    = 1/2;
lim_BER = 10e-5;

% Modulation
m = 4;                 % Modulation order QPSK
k = log2(m);            % Bits per symbo

% 'Frame' size
numofbitsperpacket=100;  % number of bits in each user packet
maxnumofdatabits=1e6;
nbrOfMonteCarloRealizations=maxnumofdatabits/numofbitsperpacket;

% Results
totalBERMR    = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );
%totalBERMMSE  = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );
totalBERZF    = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );
min_EbNo      = NaN( length(K_arr), (M_max-min(K_arr))/M_incr );

% Pro-coding names

procoding_names = {'MR', 'ZF'};

%{
  Simulation loop
%}
 % Start timer
tic
for i_k=1:length(K_arr) %For each set of number of UEs
    K = K_arr(i_k);
    
    disp( strcat(num2str(K), ' users') )
    
    
    
    M_arr = K:M_incr:M_max;
    for i_m=1:length(M_arr); %For each set of number of antennas
        M = M_arr(i_m);
        disp( strcat(' - ', num2str(M), ' antennas') )
        
        for i_EbNo=1:length(EbNo_arr); % While SNR not meet
            
            EbNo = EbNo_arr(i_EbNo);
            
           
            % BER calculation starts here
            totalErrsMR=0;
            %sumBERMMSE=0;
            totalErrsZF=0;
            
            %generate noise
            %Convert Eb/No to SNR
            snrdB = EbNo + 10*log10(k*rate);
            Txpwr=2;
            snrlin=10^(snrdB/10);
            N0=Txpwr/snrlin;
            noiseSigma=sqrt(N0/2);  %Standard deviation for AWGN Noise when x is complex
            
            parfor n=1:nbrOfMonteCarloRealizations;
                
                numErrsSoftMR=0;
                %numErrsSoftMMSE=0;
                numErrsSoftZF=0;
                
                %Generate binary data and convert to symbols
                dataIn = randi([0 1],numofbitsperpacket,K);
                
                dataEnc=zeros((1/rate)*numofbitsperpacket,K); % initialize for coding
                % Convolutionally encode the data
                for i=1:K
                    
                    dataEnc(:,i) = convenc(dataIn(:,i),trellis);
                    
                end
                
                % QAM modulate
                txSig = qammod(dataEnc,m,'InputType','bit');
                
                
                %Generate channel gain
                H=(randn(M,K,1)+1i*randn(M,K,1)).*sqrt(1/2);
                
                % generate noise
                noise = (noiseSigma.*(randn(1,M)+1i*randn(1,M))).';%computed noise
                
                %received signal
                y=H*txSig.' + noise;

                %Compute normalized MR precoding
                conjH=H';
                conjHH=conjH*H;
                
                wMR=conjH;
                yMR=wMR*y;
                recMR=yMR;
                
                %Compute normalized MMSE precoding
                %wMMSE=(H'*H + (1/(10.^(snrdB/10)))*eye(K))\H';
                %yMMSE=wMMSE*y;
                %recMMSE=yMMSE;
                
                %Compute normalized ZF precoding
                wZF=(conjHH)\conjH;
                yZF=wZF*y;
                recZF=yZF;
                
                
                % Tranpose correct the size for demodulation
                recMR=recMR.';
                %recMMSE=recMMSE.';
                recZF=recZF.';
                
                %Demodulation soft demode
                rxDataSoftMR = qamdemod(recMR,m,'OutputType','approxllr', ...
                    'NoiseVariance',10.^(snrdB/10));
                %rxDataSoftMMSE = qamdemod(recMMSE(:,i),m,'OutputType','approxllr', ...
                %   'NoiseVariance',10.^(snrdB/10));
                rxDataSoftZF = qamdemod(recZF,m,'OutputType','approxllr', ...
                    'NoiseVariance',10.^(snrdB/10));
                
                %Decode with Viterbi soft decoding user by user
                for i=1:K
                    dataSoftMR= vitdec(rxDataSoftMR(:,i),trellis,tbl,'cont','unquant');
                    %dataSoftMMSE= vitdec(rxDataSoftMMSE,trellis,tbl,'cont','unquant');
                    dataSoftZF = vitdec(rxDataSoftZF(:,i),trellis,tbl,'cont','unquant');
                    
                    % Calculate the number of bit errors in the frame. Adjust for the
                    % decoding delay, which is equal to the traceback depth.
                    data=dataIn(:,i);
                    numErrsInFrameSoftMRpuser  = biterr(data(1:end-tbl),dataSoftMR(tbl+1:end));
                    %numErrsInFrameSoftMMSE  = biterr(data(1:end-tbl),dataSoftMMSE(tbl+1:end));
                    numErrsInFrameSoftZFpuser  = biterr(data(1:end-tbl),dataSoftZF(tbl+1:end));
                    
                    % Increment the error and bit counters
                    numErrsSoftMR = numErrsSoftMR + numErrsInFrameSoftMRpuser;
                    %numErrsSoftMMSE= numErrsSoftMMSE + numErrsInFrameSoftMMSE;
                    numErrsSoftZF = numErrsSoftZF + numErrsInFrameSoftZFpuser;
                    
                end
                
                totalErrsMR=totalErrsMR+ numErrsSoftMR;
                %sumBERMMSE=sumBERMMSE+ numErrsSoftMMSE;
                totalErrsZF=totalErrsZF+ numErrsSoftZF;
                
%                 numErrsSoftMR=0;
%                 %numErrsSoftMMSE=0;
%                 numErrsSoftZF=0;
                
            end
            %% BER calculation ends here
            BERMR(i_m, i_EbNo) = totalErrsMR/maxnumofdatabits/K;
            %BERMMSE = sumBERMMSE/maxnumofdatabits/K;
            BERZF(i_m, i_EbNo) = totalErrsZF/maxnumofdatabits/K;
            
           disp(sprintf('\t +  BER with ZF at %i antennas with a SNR of %i dB is  %0.5e', M, EbNo, BERMR(i_m, i_EbNo)))
           disp(sprintf('\t +  BER with MR at %i antennas with a SNR of %i dB is  %0.5e', M, EbNo, BERZF(i_m, i_EbNo)))
           
           disp('-------------------------------------------------------ER 1.41450e-03.----')
           
    
           % [achived_BER, type] = min([BERMR, BERZF]); % We should pick one or evaluate each one separatly.
            
            % Check termination conditions
%             if achived_BER <= lim_BER;
%                 % Announc success
%                 disp( strcat(num2str(M),':Antennas, ', num2str(K),': Users at: ',num2str(EbNo),'dB.') )
%                 disp( strcat('- Pre-coding:', procoding_names(1), ', BER:', num2str(BERMR) ));
%                 %disp( strcat('- Pre-coding:', procoding_names(2), ', BER:', num2str(BERMMSE) ));
%                 disp( strcat('- Pre-coding:', procoding_names(3), ', BER:', num2str(BERZF) ));
%                 
%                 % Save values
%                 totalBERMR(i_k, i_m) = BERMR;
%                 %totalBERMMSE(i_k, i_m) = BERMMSE;
%                 totalBERZF(i_k, i_m) = BERZF;
%                 min_EbNo(i_k, i_m) = EbNo;
%                 
%                 % Strop itteration
%                 break
%             else
%                 disp( strcat(' - ', num2str(M),':Antennas, ', num2str(K),':Users. BER(', num2str(achived_BER) , ') limit NOT reached') )
%             end
            
        end
        
    end
    
end
toc

%%Better picturing
% for l=1:length(Mant)
%     if totalBERMMSE(l)==0
%         totalBERMMSE(l)=10^-9;
%     end
%     if totalBERZF(l)==0
%         totalBERZF(l)=10^-9;
%     end
% end

%Plot simulation results
% range=M_arr;
% figure; hold on; box on;
% set(gca, 'YScale', 'log')
% plot(range,totalBERMR,'k--','LineWidth',1);
% plot(range,totalBERMMSE,'k-s','LineWidth',1);
% plot(range,totalBERZF,'ro-','LineWidth',1);
% grid on
% legend('MR','MMSE','ZF','Location','NorthEast');
%
% xlabel('Number of BS Antennas (M)');
% ylabel('Bit Error Rate (BER)');
% title('K=10, SNR=5 dB')
% ylim([0 0.5]);