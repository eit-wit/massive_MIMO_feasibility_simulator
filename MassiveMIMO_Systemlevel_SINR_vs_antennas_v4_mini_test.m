function [results] = MassiveMIMO_Systemlevel_SINR_vs_antennas_v4_mini_test()
    rng default
    rng('shuffle');
    format long
    
    %{
      Simulation parameters
    %}

    % UEs
    K_arr       = [10];
    % SNR
    EbNo_arr    = [10]; % Start value
    % Antennas
    %M_init      = linspace(10, 100, length(EbNo_arr));
    M_min_zf = 20;
    M_incr = 5;

    %{
      System parameters
    %}
    %Convolutional code rate 1/2 and length 7
    trellis = poly2trellis(7,[171 133]);
    tbl     = 32;
    rate    = 1/2;
    lim_BER = 1e-6;

    % Modulation
    m = 4;                 % Modulation order QPSK
    k = log2(m);            % Bits per symbo

    % 'Frame' size
    numofbitsperpacket = 100;  % number of bits in each user packet
    maxnumofdatabits = 1e3;%2*1e7;
    nbrOfMonteCarloRealizations = maxnumofdatabits/numofbitsperpacket;

    % Result data strcture(s)
    results = struct('MR', [], 'ZF', []);
    elapsed_time_array = [];

    %{
      Simulation loop
    %}
    %disp(sprintf('Starting Massive MIMO simulator, target BER: %0.0e, and %0.0e bits', lim_BER, maxnumofdatabits))
    for i_EbNo=1:length(EbNo_arr)
        EbNo = EbNo_arr(i_EbNo);

        for i_k=1:length(K_arr) % Nbr users
            K = K_arr(i_k);

            precoding_schemes = struct('MR', true, 'ZF', true);

            M = K; % = M_min_zf;

            while precoding_schemes.ZF || precoding_schemes.MR
                tic;

                [ber_zf, ber_mr, aborted] = massive_mimo_ber_for_snr_vs_antennas(EbNo, M, K, numofbitsperpacket, maxnumofdatabits, k, rate, trellis, tbl, m, precoding_schemes, nbrOfMonteCarloRealizations);

                elapsed_time_array(end+1) = toc;

                if precoding_schemes.ZF
                    %M_min_zf = M;
                    results.ZF = [results.ZF ; K, EbNo, ber_zf, M];
                end
                if precoding_schemes.MR
                    results.MR = [results.MR ; K, EbNo, ber_mr, M];
                end

                % ZF
                if ber_zf <= lim_BER
                    %disp(sprintf('\t + Reached target BER for ZF at %i antennas with a BER of %0.5e', M, ber_zf))
                    precoding_schemes.ZF = false;
                %else
                    %disp(sprintf('\t - Failed to reach target BER for ZF. BER %0.5e.', ber_zf))
                end

                % MR
                if ber_mr <= lim_BER
%                     disp(sprintf('\t + Reached target BER for MR at %i antennas with a BER of %0.0e', M, ber_mr))
                    precoding_schemes.MR = false;
%                 else
%                     disp(sprintf('\t - Failed to reach target BER for MR. BER %0.5e.', ber_mr))
                end

                if precoding_schemes.ZF || precoding_schemes.MR
                    M = M + M_incr;
%                     disp(sprintf('\t -> Increasing the number of antennas to %i', M))
                end

%                 save( sprintf('SINR_vs_antennas_results_BER:%1.0e_bits:%1.0e-%s.mat', lim_BER, maxnumofdatabits, datetime('today')), 'results' );
            end

            disp(' ----------------------------------------------------------- ')
        end
    end

%     save( sprintf('execution_times-%s.mat', datetime('today')), 'elapsed_time_array' );

    %histogram(elapsed_time_array)
end
