function [results, elapsed_time_array, M_min_zf, M_min_mr] = MassiveMIMO_Systemlevel_SINR_vs_antennas_v5(EbNo, M_start, M_incr, K, lim_BER, numofbitsperpacket, maxnumofdatabits, precoding_schemes, N)

    % Result data strcture(s)
    results = struct('MR', [], 'ZF', []);
    elapsed_time_array = [];
    
    M_min_zf = M_start;
    M_min_mr = M_start;
    M = M_start;
        
    while precoding_schemes.ZF || precoding_schemes.MR
        tic;

        [ber_zf, ber_mr, aborted] = massive_mimo_ber_for_snr_vs_antennas_v2(EbNo, M, K, numofbitsperpacket, maxnumofdatabits, precoding_schemes, N);

        elapsed_time_array(end+1) = toc;
        
        disp(sprintf('\t (!) %f s', elapsed_time_array(end)))

        % ZF
        if precoding_schemes.ZF
            M_min_zf = M;
            results.ZF = [results.ZF ; K, EbNo, ber_zf, M];
            
             if ber_zf <= lim_BER
                disp(sprintf('\t + Reached target BER for ZF at %i antennas with a BER of %0.5e', M, ber_zf))
                precoding_schemes.ZF = false;
            else
                disp(sprintf('\t - Failed to reach target BER for ZF. BER %0.5e.', ber_zf))
            end
        end
        % MR
        if precoding_schemes.MR
            M_min_mr = M;
            results.MR = [results.MR ; K, EbNo, ber_mr, M];
            
            if ber_mr <= lim_BER
                disp(sprintf('\t + Reached target BER for MR at %i antennas with a BER of %0.0e', M, ber_mr))
                precoding_schemes.MR = false;
            else
                disp(sprintf('\t - Failed to reach target BER for MR. BER %0.5e.', ber_mr))
            end
        end

        if precoding_schemes.ZF || precoding_schemes.MR
            M = M + M_incr;
            disp(sprintf('\t -> Increasing the number of antennas to %i', M))
        end

    end

    save( sprintf('SINR_vs_antennas_results-K:%i_M:%i_EdNo:%i->%s.mat', K, M, EbNo, datetime('today')), 'results' );
    %save( sprintf('execution_times-K:%i_M:%i_EdNo:%i->%s.mat', K, M, EbNo, datetime('today')), 'elapsed_time_array' );
    disp(' ----------------------------------------------------------- ')
    
end



%histogram(elapsed_time_array)

%%Better picturing
% for l=1:length(Mant)
%     if totalBERMMSE(l)==0
%         totalBERMMSE(l)=10^-9;
%     end
%     if totalBERZF(l)==0
%         totalBERZF(l)=10^-9;
%     end
% end

%%Plot simulation results
% range=M_arr;
% figure; hold on; box on;
% set(gca, 'YScale', 'log')
% plot(range,totalBERMR,'k--','LineWidth',1);
% plot(range,totalBERMMSE,'k-s','LineWidth',1);
% plot(range,totalBERZF,'ro-','LineWidth',1);
% grid on
% legend('MR','MMSE','ZF','Location','NorthEast');
%
% xlabel('Number of BS Antennas (M)');
% ylabel('Bit Error Rate (BER)');
% title('K=10, SNR=5 dB')
% ylim([0 0.5]);