clear;
clc
close all
rng default
rng('shuffle');
format long

c = parcluster(); 

%{
  Simulation parameters
%}

% UEs
K_arr       = [10];
% SNR
EbNo_arr    = 10:-1:-15; % Start value
% Antennas
%M_init      = linspace(10, 100, length(EbNo_arr));
M_min_zf = 10;
M_incr = 5;

%{
  System parameters
%}
%Convolutional code rate 1/2 and length 7
trellis = poly2trellis(7,[171 133]);
tbl     = 32;
rate    = 1/2;
lim_BER = 1e-5;

% Modulation
m = 4;                 % Modulation order QPSK
k = log2(m);            % Bits per symbo

% 'Frame' size
numofbitsperpacket=100;  % number of bits in each user packet
maxnumofdatabits=1e6;
nbrOfMonteCarloRealizations=maxnumofdatabits/numofbitsperpacket;

% Result data strcture(s)
results = struct('MR', [], 'ZF', []);

%{
  Simulation loop
%}
disp(sprintf('Starting Massive MIMO simulator with %i workers, target BER: %0.0e, and %0.0e bits', c.NumWorkers, lim_BER, maxnumofdatabits))
for i_k=1:length(K_arr)
    K = K_arr(i_k);
    
    for i_EbNo=1:length(EbNo_arr)
        EbNo = EbNo_arr(i_EbNo);
    
        ber_zf = realmax;
        ber_mr = realmax;

        precoding_schemes = struct('MR', true, 'ZF', true);

        M = M_min_zf;
        
        while precoding_schemes.ZF || precoding_schemes.MR 

            [ber_zf, ber_mr, aborted] = massive_mimo_ber_for_snr_vs_antennas(EbNo, M, K, numofbitsperpacket, maxnumofdatabits, k, rate, trellis, tbl, m, precoding_schemes, nbrOfMonteCarloRealizations);

            if precoding_schemes.ZF
                M_min_zf = M;
                results.ZF = [results.ZF ; K, EbNo, ber_zf, M];
            end

            results.MR = [results.MR ; K, EbNo, ber_mr, M];

            % ZF
            if ber_zf <= lim_BER
                disp(sprintf('\t + Reached target BER for ZF at %i antennas with a BER of %0.5e', M, ber_zf))
                precoding_schemes.ZF = false;
            else
                disp(sprintf('\t - Failed to reach target BER for ZF. BER %0.5e.', ber_zf))
            end
            
            % MR
            if ber_mr <= lim_BER
                disp(sprintf('\t + Reached target BER for MR at %i antennas with a BER of %0.0e', M, ber_mr))
                precoding_schemes.MR = false;
            else
                disp(sprintf('\t - Failed to reach target BER for MR. BER %0.5e.', ber_mr))
            end
            
            if precoding_schemes.ZF || precoding_schemes.MR
                M = M + M_incr;
                disp(sprintf('\t -> Increasing the number of antennas to %i', M))
            end
        end

        disp(' ----------------------------------------------------------- ')
    end
end

save( sprintf('SINR_vs_antennas_results_BER:%e_UEs:%i_bits:%e-%s', lim_BER, K, maxnumofdatabits, datetime('today')), 'results' );

%%Better picturing
% for l=1:length(Mant)
%     if totalBERMMSE(l)==0
%         totalBERMMSE(l)=10^-9;
%     end
%     if totalBERZF(l)==0
%         totalBERZF(l)=10^-9;
%     end
% end

%%Plot simulation results
% range=M_arr;
% figure; hold on; box on;
% set(gca, 'YScale', 'log')
% plot(range,totalBERMR,'k--','LineWidth',1);
% plot(range,totalBERMMSE,'k-s','LineWidth',1);
% plot(range,totalBERZF,'ro-','LineWidth',1);
% grid on
% legend('MR','MMSE','ZF','Location','NorthEast');
%
% xlabel('Number of BS Antennas (M)');
% ylabel('Bit Error Rate (BER)');
% title('K=10, SNR=5 dB')
% ylim([0 0.5]);