clear;
clc
close all
rng default
rng('shuffle');
format long

c = parcluster(); 

%{
  Simulation parameters
%}
% UEs
K_arr       = 10:10:200;
% SNR
EbNo_arr    = 10:-1:-15; % Start value
% Antennas
M_incr      = 5;
M_max       = max(K_arr);



%{
  System parameters
%}
%Convolutional code rate 1/2 and length 7
trellis = poly2trellis(7,[171 133]);
tbl     = 32;
rate    = 1/2;
lim_BER = 1e-5;

% Modulation
m = 4;                 % Modulation order QPSK
k = log2(m);           % Bits per symbo

% 'Frame' size
numofbitsperpacket=100;  % number of bits in each user packet
maxnumofdatabits=1e6;
nbrOfMonteCarloRealizations=maxnumofdatabits/numofbitsperpacket;

% Result data strcture(s)
results = struct('MR', [], 'ZF', []);
elapsed_time_array = [];

%{
  Simulation loop
%}
disp(sprintf('Starting Massive MIMO simulator with %i workers, target BER: %0.0e, and %0.0e bits', c.NumWorkers, lim_BER, maxnumofdatabits))
for i_k=1:length(K_arr)
    K = K_arr(i_k);

    for i_EbNo=1:length(EbNo_arr)
        EbNo = EbNo_arr(i_EbNo);
        
        M_arr = K:M_incr:M_max;
        for i_m = 1:length(M_arr);
            M = M_arr(i_m);
            
            tic;
            
            [ber_zf, ber_mr, aborted] = massive_mimo_ber_for_snr_vs_antennas(EbNo, M, K, numofbitsperpacket, maxnumofdatabits, k, rate, trellis, tbl, m, struct('MR', true, 'ZF', true), nbrOfMonteCarloRealizations);
        
            elapsed_time_array(end+1) = toc;
            
            results.ZF = [results.ZF; K, EbNo, ber_zf, M];
            results.MR = [results.MR; K, EbNo, ber_mr, M];
        end
    end
end

save( sprintf('sweep_BER:%e_bits:%e-%s.mat', lim_BER, maxnumofdatabits, datetime('today')), 'results' );

histogram(elapsed_time_array)

%%Better picturing
% for l=1:length(Mant)
%     if totalBERMMSE(l)==0
%         totalBERMMSE(l)=10^-9;
%     end
%     if totalBERZF(l)==0
%         totalBERZF(l)=10^-9;
%     end
% end

%%Plot simulation results
% range=M_arr;
% figure; hold on; box on;
% set(gca, 'YScale', 'log')
% plot(range,totalBERMR,'k--','LineWidth',1);
% plot(range,totalBERMMSE,'k-s','LineWidth',1);
% plot(range,totalBERZF,'ro-','LineWidth',1);
% grid on
% legend('MR','MMSE','ZF','Location','NorthEast');
%
% xlabel('Number of BS Antennas (M)');
% ylabel('Bit Error Rate (BER)');
% title('K=10, SNR=5 dB')
% ylim([0 0.5]);