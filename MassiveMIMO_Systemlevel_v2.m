clear;
clc
close all
rng default
rng('shuffle');
format long

m = 4;                 % Modulation order QPSK
k = log2(m);            % Bits per symbol

K=1;   %number of users
Mant=2; %number of antennas
numofbitsperpacket=100;  % number of bits in each user packet
maxnumofdatabits=1e6;
nbrOfMonteCarloRealizations=maxnumofdatabits/numofbitsperpacket;
EbNoVec = 2;       % Eb/No values (dB)


%Convolutional code rate 1/2 and length 7
trellis = poly2trellis(7,[171 133]);
tbl = 32;
rate = 1/2;

sumBERMR=zeros(length(Mant),1);
sumBERMMSE=zeros(length(Mant),1);
sumBERZF=zeros(length(Mant),1);


for l=1:length(Mant)
    
    M=Mant(l)
    
    %Generate of Rayleigh fading channel realizations
    Hall = (randn(M,K,nbrOfMonteCarloRealizations)+1i*randn(M,K,nbrOfMonteCarloRealizations)).*sqrt(1/2);
    
    numErrsSoftMR=0;
    numErrsSoftMMSE=0;
    numErrsSoftZF=0;
    numofdatabitssent=0;
    n=0;
    totalBERMR=0;
    totalBERMMSE=0;
    totalBERZF=0;
    while numofdatabitssent < maxnumofdatabits
        n=n+1;
        
        %Generate binary data and convert to symbols
        dataIn = randi([0 1],numofbitsperpacket,K);
        
        % Convolutionally encode the data
        for i=1:K
            dataEnc(:,i) = convenc(dataIn(:,i),trellis);
            
            % QAM modulate
            
            txSig(:,i) = qammod(dataEnc(:,i),m,'InputType','bit');
        end
        %txSig=txSig.';
        
        % Convert Eb/No to SNR
        snrdB = EbNoVec + 10*log10(k*rate);
        
        %Go through all channel realizations
        %        for n = 1:nbrOfMonteCarloRealizations
        
        %Extract the current channel realization
        H = Hall(:,:,n);
        
        %Received signal
        recMMSE=zeros(K,length(txSig));
        recZF=zeros(K,length(txSig));
        recMR=zeros(K,length(txSig));
        
        for i=1:length(txSig)
            %y=H*txSig(:,i) ;
            ytx=txSig(i,:).';
            %ytx=ytx.';
            
            %generate noise
            Txpwr=mean(abs(ytx).^2);
            snrlin=10^(snrdB/10);
            N0=Txpwr/snrlin;
            noiseSigma=sqrt(N0/2);  %Standard deviation for AWGN Noise when x is complex
            noise = (noiseSigma*(randn(1,M)+1i*randn(1,M))).';%computed noise
            %noisepwr=mean(abs(noise).^2);
            %avgsnr=avgsnr + 10*log10(Txpwr/noisepwr);
            y=H*ytx + noise;
            
            
            
            %Compute  MR precoding
            % wMR = H'./repmat(sqrt(sum(abs(H').^2,1)),[M 1]);
            wMR=H';
            yMR=wMR*y;
            recMR(:,i)=yMR;
            
            %recMR(:,i)=ytx + noise;
            
            %Compute normalized MMSE precoding
            wMMSE=(H'*H + (1/(10.^(snrdB/10)))*eye(K))\H';
            yMMSE=wMMSE*y;
            recMMSE(:,i)=yMMSE;
            
            %Compute normalized ZF precoding
            wZF=(H'*H)\H';
            yZF=wZF*y;
            recZF(:,i)=yZF;
            
        end
        
        recMR=recMR.';
        recMMSE=recMMSE.';
        recZF=recZF.';
        
        numErrsInFrameSoftMR=0;
        numErrsInFrameSoftMMSE=0;
        numErrsInFrameSoftZF=0;
        
        for i=1:K
            
            %Demodulation without coding
            % rxDataSoftMR = qamdemod(recMR(:,i),m,'OutputType','bit');
            % rxDataSoftMMSE = qamdemod(recMMSE(:,i),m,'OutputType','bit');
            % rxDataSoftZF = qamdemod(recZF(:,i),m,'OutputType','bit');
            % data=dataIn(:,i);
            % numErrsInFrameSoftMR  = biterr(data,rxDataSoftMR);
            % numErrsInFrameSoftMMSE  = biterr(data,rxDataSoftMMSE);
            % numErrsInFrameSoftZF  = biterr(data,rxDataSoftZF);
            
            %Demodulation with coding
            rxDataSoftMR = qamdemod(recMR(:,i),m,'OutputType','approxllr', ...
                'NoiseVariance',10.^(snrdB/10));
            rxDataSoftMMSE = qamdemod(recMMSE(:,i),m,'OutputType','approxllr', ...
                'NoiseVariance',10.^(snrdB/10));
            rxDataSoftZF = qamdemod(recZF(:,i),m,'OutputType','approxllr', ...
                'NoiseVariance',10.^(snrdB/10));
            
            %Decode with Viterbi soft decoding
            dataSoftMR= vitdec(rxDataSoftMR,trellis,tbl,'cont','unquant');
            dataSoftMMSE= vitdec(rxDataSoftMMSE,trellis,tbl,'cont','unquant');
            dataSoftZF = vitdec(rxDataSoftZF,trellis,tbl,'cont','unquant');
            %
            % Calculate the number of bit errors in the frame. Adjust for the
            % decoding delay, which is equal to the traceback depth.
            
            % numErrsInFrameHard = biterr(dataIn(1:end-tbl),dataHard(tbl+1:end));
            data=dataIn(:,i);
            numErrsInFrameSoftMR  = biterr(data(1:end-tbl),dataSoftMR(tbl+1:end));
            numErrsInFrameSoftMMSE  = biterr(data(1:end-tbl),dataSoftMMSE(tbl+1:end));
            numErrsInFrameSoftZF  = biterr(data(1:end-tbl),dataSoftZF(tbl+1:end));
            
            % Increment the error and bit counters
            %numErrsHard = numErrsHard + numErrsInFrameHard;
            numErrsSoftMR = numErrsSoftMR + numErrsInFrameSoftMR;
            numErrsSoftMMSE = numErrsSoftMMSE + numErrsInFrameSoftMMSE;
            numErrsSoftZF = numErrsSoftZF + numErrsInFrameSoftZF;
            
            %           end
            
        end
        
        totalBERMR=totalBERMR+ numErrsSoftMR;
        totalBERMMSE=totalBERMMSE+ numErrsSoftMMSE;
        totalBERZF=totalBERZF+ numErrsSoftZF;
        
        numErrsSoftMR=0;
        numErrsSoftMMSE=0;
        numErrsSoftZF=0;
        numofdatabitssent=numofdatabitssent + numofbitsperpacket;
    end
    
    sumBERMR(l)=totalBERMR + sumBERMR(l);
    sumBERMMSE(l)=totalBERMMSE + sumBERMMSE(l);
    sumBERZF(l)=totalBERZF + sumBERZF(l);
    
end

avgtotalBERMR= sumBERMR./maxnumofdatabits/K
avgtotalBERMMSE= sumBERMMSE./maxnumofdatabits/K
avgtotalBERZF= sumBERZF./maxnumofdatabits/K

%Plot simulation results
range=Mant;
figure; hold on; box on;
set(gca, 'YScale', 'log')
plot(range,avgtotalBERMR,'k--','LineWidth',1);
plot(range,avgtotalBERMMSE,'k-s','LineWidth',1);
plot(range,avgtotalBERZF,'ro-','LineWidth',1);
grid on
legend('MR','MMSE','ZF','Location','NorthEast');

xlabel('Number of BS Antennas (M)');
ylabel('Bit Error Rate (BER)');
title('K=10, SNR=5 dB')
ylim([0 0.5]);
