clear;
clc
close all
rng default
rng('shuffle');
format long

% Parametrization
m = 4;                 % Modulation order QPSK
k = log2(m);            % Bits per symbol

K=10;   %number of users
Mant=[20]; %number of antennas
numofbitsperpacket=100;  % number of bits in each user packet
maxnumofdatabits=1e7;
nbrOfMonteCarloRealizations=maxnumofdatabits/numofbitsperpacket;
EbNoVec = 20;       % Eb/No values (dB)


%Convolutional code rate 1/2 and length 7
trellis = poly2trellis(7,[171 133]);
tbl = 32;
rate = 1/2;

sumBERMR=zeros(length(Mant),1);
%sumBERMMSE=zeros(length(Mant),1);
sumBERZF=zeros(length(Mant),1);

tic
for l=1:length(Mant)
    
    M=Mant(l)
    
    totalBERMR=0;
    %totalBERMMSE=0;
    totalBERZF=0;
    
    snrdB = EbNoVec + 10*log10(k*rate);
    Txpwr=2;  % this must be dynamic
    snrlin=10^(snrdB/10);
    N0=Txpwr/snrlin;
    noiseSigma=sqrt(N0/2);
    
    parfor n=1:nbrOfMonteCarloRealizations
        
        numErrsSoftMR=0;
        %numErrsSoftMMSE=0;
        numErrsSoftZF=0;
        
        
        %Generate binary data and convert to symbols
        dataIn = randi([0 1],numofbitsperpacket,K);
        
        
        dataEnc=zeros((1/rate)*numofbitsperpacket,K); % initialize for coding
        % Convolutionally encode the data
        for i=1:K
            
            dataEnc(:,i) = convenc(dataIn(:,i),trellis);
            
        end
%         
        % QAM modulate
        txSig = qammod(dataEnc,m,'InputType','bit');
        
        
        %Generate channel realization
        H=(randn(M,K,1)+1i*randn(M,K,1)).*sqrt(1/2);
        
        % generate noise
        noise = (noiseSigma.*(randn(1,M)+1i*randn(1,M))).';%computed noise
        
        %received signal
        y=H*txSig.' + noise.*ones(1,length(txSig));
        
        %Compute  MR precoding
        % wMR = H'./repmat(sqrt(sum(abs(H').^2,1)),[M 1]);
        conjH=H';
        conjHH=conjH*H;
        
        
        wMR=conjH;
        yMR=wMR*y;
        recMR=yMR;
        
        %recMR(:,i)=ytx + noise;
        
        %Compute normalized MMSE precoding
        %         wMMSE=(conjHH + (1/(10.^(snrdB/10)))*eye(K))\conjH;
        %         yMMSE=wMMSE*y;
        %         recMMSE=yMMSE;
        
        %Compute normalized ZF precoding
        wZF=(conjHH)\conjH;
        yZF=wZF*y;
        recZF=yZF;
        
        
        recMR=recMR.';
        %recMMSE=recMMSE.';
        recZF=recZF.';
        
        numErrsInFrameSoftMR=0;
        %numErrsInFrameSoftMMSE=0;
        numErrsInFrameSoftZF=0;
        
        
        
        %Demodulation without coding
%         rxDataSoftMR = qamdemod(recMR,m,'OutputType','bit');
%         %rxDataSoftMMSE = qamdemod(recMMSE,m,'OutputType','bit');
%         rxDataSoftZF = qamdemod(recZF,m,'OutputType','bit');
%         data=dataIn;
%         numErrsInFrameSoftMR  = biterr(data,rxDataSoftMR);
%         %numErrsInFrameSoftMMSE  = biterr(data,rxDataSoftMMSE);
%         numErrsInFrameSoftZF  = biterr(data,rxDataSoftZF);
%         
%         
%         totalBERMR=totalBERMR+ numErrsInFrameSoftMR ;
%         %totalBERMMSE=totalBERMMSE+ numErrsSoftMMSE;
%         totalBERZF=totalBERZF+ numErrsInFrameSoftZF;
        %Demodulation with coding
        
        rxDataSoftMR = qamdemod(recMR,m,'OutputType','approxllr', ...
            'NoiseVariance',10.^(snrdB/10));
        %rxDataSoftMMSE = qamdemod(recMMSE,m,'OutputType','approxllr', ...
        %            'NoiseVariance',10.^(snrdB/10));
        rxDataSoftZF = qamdemod(recZF,m,'OutputType','approxllr', ...
            'NoiseVariance',10.^(snrdB/10));
        
        for i=1:K
            %Decode with Viterbi soft decoding
            
            dataSoftMR= vitdec(rxDataSoftMR(:,i),trellis,tbl,'cont','unquant');
            %dataSoftMMSE= vitdec(rxDataSoftMMSE(:,i),trellis,tbl,'cont','unquant');
            dataSoftZF = vitdec(rxDataSoftZF(:,i),trellis,tbl,'cont','unquant');
            
            %
            % Calculate the number of bit errors in the frame. Adjust for the
            % decoding delay, which is equal to the traceback depth.
            
            % numErrsInFrameHard = biterr(dataIn(1:end-tbl),dataHard(tbl+1:end));
            data=dataIn(:,i);
            numErrsInFrameSoftMR  = biterr(data(1:end-tbl),dataSoftMR(tbl+1:end));
            %numErrsInFrameSoftMMSE  = biterr(data(1:end-tbl),dataSoftMMSE(tbl+1:end));
            numErrsInFrameSoftZF  = biterr(data(1:end-tbl),dataSoftZF(tbl+1:end));
            
            % Increment the error and bit counters
            %numErrsHard = numErrsHard + numErrsInFrameHard;
            numErrsSoftMR = numErrsSoftMR + numErrsInFrameSoftMR;
            %numErrsSoftMMSE = numErrsSoftMMSE + numErrsInFrameSoftMMSE;
            numErrsSoftZF = numErrsSoftZF + numErrsInFrameSoftZF;
            
            %           end
            
        end
        
        totalBERMR=totalBERMR+ numErrsSoftMR;
        %totalBERMMSE=totalBERMMSE+ numErrsSoftMMSE;
        totalBERZF=totalBERZF+ numErrsSoftZF;
        
        
    end
    
    sumBERMR(l)=totalBERMR + sumBERMR(l);
    %sumBERMMSE(l)=totalBERMMSE + sumBERMMSE(l);
    sumBERZF(l)=totalBERZF + sumBERZF(l);
    
end
toc
avgtotalBERMR= sumBERMR./maxnumofdatabits/K
%avgtotalBERMMSE= sumBERMMSE./maxnumofdatabits/K
avgtotalBERZF= sumBERZF./maxnumofdatabits/K

%Plot simulation results
range=Mant;
figure; hold on; box on;
set(gca, 'YScale', 'log')
plot(range,avgtotalBERMR,'k--','LineWidth',1);
%plot(range,avgtotalBERMMSE,'k-s','LineWidth',1);
plot(range,avgtotalBERZF,'ro-','LineWidth',1);
grid on
legend('MR','ZF','Location','NorthEast');

xlabel('Number of BS Antennas (M)');
ylabel('Bit Error Rate (BER)');
title('K=10, SNR=5 dB')
ylim([0 0.5]);
