function [ber_zf, ber_mr, aborted] = massive_mimo_ber_for_snr_vs_antennas_v2(EbNo, M, K, numofbitsperpacket, maxnumofdatabits, pre_code_scheme, N)
    disp( sprintf(' - Running BER simulation for; %i users, %i antennas, at an SNR of %0.2d dB', K, M, EbNo) )
    
    %Convolutional code rate 1/2 and length 7
    trellis = poly2trellis(7,[171 133]);
    tbl     = 32;
    rate    = 1/2;
    
    % Modulation
    m = 4;                 % Modulation order QPSK
    k = log2(m);            % Bits per symbo
    
    % BER calculation starts here
    totalErrsZF = 0;
    totalErrsMR = 0;
    
    % Number of simulations
    nbrOfMonteCarloRealizations=maxnumofdatabits/numofbitsperpacket;
    
    % Generate noise
    % Convert Eb/No to SNR
    snrdB = EbNo + 10*log10(m*rate);
    Txpwr=2;
    snrlin=10^(snrdB/10);
    N0=Txpwr/snrlin;
    noiseSigma=sqrt(N0/2);  %Standard deviation for AWGN Noise when x is complex
    
    parfor n=1:nbrOfMonteCarloRealizations/N
        totalErrsZF_reali = 0;
        totalErrsMR_reali = 0;
        
        for j=1:N
            numErrsSoftZF_batch = 0;
            numErrsSoftMR_batch = 0;
            
            % Generate binary data and convert to symbols
            dataIn = randi([0 1],numofbitsperpacket,K);

            dataEnc=zeros((1/rate)*numofbitsperpacket,K); % initialize for coding
            % Convolutionally encode the data
            for i=1:K
                dataEnc(:,i) = convenc(dataIn(:,i),trellis);
            end

            % QAM modulate
            txSig = qammod(dataEnc,m,'InputType','bit');

            % Generate channel gain
            H=(randn(M,K,1)+1i*randn(M,K,1)).*sqrt(1/2);

            % Generate noise
            noise = (noiseSigma.*(randn(1,M)+1i*randn(1,M))).';%computed noise

            % Received signal
            y=H*txSig.' + repmat(noise, 1, length(txSig));

            % Compute normalized MR precoding
            conjH=H';
            conjHH=conjH*H;

            if pre_code_scheme.ZF
                wZF=(conjHH)\conjH;
                yZF=wZF*y;
                recZF=yZF;
                recZF=recZF.';
                rxDataSoftZF = qamdemod(recZF,m,'OutputType','approxllr', ...
                    'NoiseVariance',10.^(snrdB/10));
            end

            if pre_code_scheme.MR
                wMR=conjH;
                yMR=wMR*y;
                recMR=yMR;
                recMR=recMR.';
                rxDataSoftMR = qamdemod(recMR, m,'OutputType','approxllr', ...
                    'NoiseVariance',10.^(snrdB/10));
            end

            % Decode with Viterbi soft decoding user by user
            for i=1:K
                data=dataIn(:,i);

                if pre_code_scheme.ZF
                    dataSoftZF = vitdec(rxDataSoftZF(:,i),trellis,tbl,'cont','unquant');
                    numErrsInFrameSoftZF  = biterr(data(1:end-tbl),dataSoftZF(tbl+1:end));
                    numErrsSoftZF_batch = numErrsSoftZF_batch + numErrsInFrameSoftZF;
                end
                
                if pre_code_scheme.MR
                    dataSoftMR = vitdec(rxDataSoftMR(:,i),trellis,tbl,'cont','unquant');
                    numErrsInFrameSoftMR  = biterr(data(1:end-tbl),dataSoftMR(tbl+1:end));
                    numErrsSoftMR_batch = numErrsSoftMR_batch + numErrsInFrameSoftMR;
                end
                
            end

            totalErrsZF_reali = totalErrsZF_reali + numErrsSoftZF_batch;
            totalErrsMR_reali = totalErrsMR_reali + numErrsSoftMR_batch;
            
        end
        
        totalErrsZF = totalErrsZF + totalErrsZF_reali;
        totalErrsMR = totalErrsMR + totalErrsMR_reali;
        
    end

    ber_zf = totalErrsZF/maxnumofdatabits/K;
    ber_mr = totalErrsMR/maxnumofdatabits/K;
    
    aborted = false;
    
end