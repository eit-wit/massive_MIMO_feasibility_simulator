snr_levels = unique(results.MR(:,2,:,:))

lim_ber = 10e-5;

min_antenna_vs_snr_mr = [];
min_antenna_vs_snr_zf = [];

% MR
figure(1)
subplot(1,2,1)
hold on; grid on;
title('MR')
xlabel('BER') % x-axis label
ylabel('# of antennas') % y-axis label
for i=1:length(snr_levels)
   snr = snr_levels(i);
   indecies = find(results.MR(:,2,:,:) == snr);
   values = results.MR(indecies, 3:4);
   plot(values(:,1), values(:,2));
   
   %find min
   min_index = min(find(values(:,1) <= lim_ber));
   min_antenna_vs_snr_mr = [min_antenna_vs_snr_mr; snr, values(min_index,2)];
end
legend('EbNo: -15 dB','EbNo: -10 dB','EbNo: -5 dB','EbNo: 0 dB','EbNo: 5 dB','EbNo: 10 dB')

% ZF
subplot(1,2,2)
hold on; grid on;
title('ZF')
xlabel('BER') % x-axis label
ylabel('# of antennas') % y-axis label
for i=1:length(snr_levels)
   snr = snr_levels(i);
   indecies = find(results.ZF(:,2,:,:) == snr);
   values = results.ZF(indecies, 3:4);
   plot(values(:,1), values(:,2));
   
   %find min
   min_index = min(find(values(:,1) <= lim_ber));
   min_antenna_vs_snr_zf = [min_antenna_vs_snr_zf; snr, values(min_index,2)];
end
legend('EbNo: -15 dB','EbNo: -10 dB','EbNo: -5 dB','EbNo: 0 dB','EbNo: 5 dB','EbNo: 10 dB')

figure(2)
hold on
grid on
plot(min_antenna_vs_snr_mr(:,1), min_antenna_vs_snr_mr(:,2))
plot(min_antenna_vs_snr_zf(:,1), min_antenna_vs_snr_zf(:,2))
xlabel('EbNo') % x-axis label
ylabel('Min # of antennas') % y-axis label
legend('MR', 'ZF')