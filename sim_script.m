function [results] = sim_script()
    rng default
    rng('shuffle');
    format long

    precoding_schemes = struct('MR', true, 'ZF', true);

    EbNo_arr = 10:-2:-15;
    M_start = 20;
    M_incr = 5;
    lim_BER = 1e-5;
    numofbitsperpacket = 100;
    maxnumofdatabits = 1e5;
    N = 10;

    results = struct('MR', [], 'ZF', []);

    for K=10:10:100
        [config_results] = config_run(EbNo_arr, K, M_incr, K, lim_BER, numofbitsperpacket, maxnumofdatabits, precoding_schemes, N);

        results.ZF = [results.ZF; config_results.ZF];
        results.MR = [results.MR; config_results.MR];

        save( sprintf('SINR_vs_antennas_results-K:%i_ZF->%s.mat', K, datetime('today')), 'results' );
    end
end

function [results] = config_run(EbNo_arr, M_start, M_incr, K, lim_BER, numofbitsperpacket, maxnumofdatabits, precoding_schemes, N)

    results = struct('MR', [], 'ZF', []);
    elapsed_time_array = [];

    for i_EbNo = 1:length(EbNo_arr)
        EbNo = EbNo_arr(i_EbNo);
        [itr_results, itr_elapsed_time_array, M_min_zf, M_min_mr] = MassiveMIMO_Systemlevel_SINR_vs_antennas_v5(EbNo, M_start, M_incr, K, lim_BER, numofbitsperpacket, maxnumofdatabits, precoding_schemes, N);

        results.ZF = [results.ZF ; itr_results.ZF];
        results.MR = [results.MR ; itr_results.MR];

        elapsed_time_array = [elapsed_time_array itr_elapsed_time_array];

        if precoding_schemes.ZF
            M_start = M_min_zf;
        else
            M_start = M_min_mr;
        end
    end

end